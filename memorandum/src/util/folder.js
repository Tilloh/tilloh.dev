export class Folder {
    constructor(id, name) {
        this.id = id;
        this.folderName = name;
        this.links = [];
    }
}