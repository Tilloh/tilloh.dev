export class Hyperlink {
    constructor(id, name, url) {
        this.id = id;
        this.linkName = name;
        this.linkUrl = url;
    }
}